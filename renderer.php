<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * course_overview block rendrer
 *
 * @package    block_course_overview
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Adapted for course_overview_uwmoodle
 *
 * @author 2013 Matt Petro
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Course_overview block rendrer
 *
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_course_overview_uwmoodle_renderer extends plugin_renderer_base {

    /** @var array $overviewids hold all activity overviewids used to write js variable */
    public $overviewids;

    /**
     * Generate course_block containing term courses
     *
     * @param array $terms array of the form $termcode => array of courses
     * @param array $overviews list of course overviews
     * @param string $selectedterm active termcode
     * @return string
     */
    public function course_block($terms, $overviews, $selectedterm) {
        $html  = '';
        $html .= $this->begin_course_block(get_string('courses', 'block_course_overview_uwmoodle'), 'uwmm_mycourses_block');
        $termcodes = array_keys($terms);
        $html .= html_writer::start_tag('div', array('class' => 'rhs'));
        $html .= $this->course_overview_allterms($terms, $overviews, $selectedterm);
        $html .= html_writer::end_tag('div');
        $html .= $this->end_course_block();
        return $html;
    }

    /**
     * Generate course_block containing non-term courses
     *
     * @param array $terms array of the form $termcode => array of courses
     * @param array $overviews list of course overviews
     * @return string
     */
    public function other_course_block($courses, $overviews) {
        $html  = '';
        $html .= $this->begin_course_block(get_string('ongoingcourses', 'block_course_overview_uwmoodle'), 'uwmm_othercourses_block', 'ongoingcourses');
        $html .= $this->course_overview($courses, $overviews, block_course_overview_uwmoodle::TERM_OTHER);
        $html .= $this->end_course_block();
        return $html;
    }

    /**
     * Generate list of all courses in all terms
     *
     * @param array $terms array of the form $termcode => array of courses
     * @param array $overviews list of course overviews
     * @param string $selectedterm active termcode
     * @return string
     */
    protected function course_overview_allterms($terms, $overviews, $selectedterm) {
        $html = '';

        $html .= html_writer::start_tag('div', array('class' => 'courselistcontainer', 'id' => 'uwmm_terms_content'));
        foreach ($terms as $termcode => $courses) {


            $termstr = block_course_overview_uwmoodle_get_term_name($termcode);
            $title = get_string('mycoursesinterm', 'block_course_overview_uwmoodle', $termstr);
            $html .= $this->output->heading($title, 2, 'termname');

            $html .= $this->course_overview($courses, $overviews, $termcode);
        }
        $html .= html_writer::end_tag('div');

        return $html;
    }

    /**
     * Generate output for a list of courses in a single term
     *
     * @param array $courses array of courses
     * @param array $overviews list of course overviews
     * @param string $termcode termcode for courses
     * @return string
     */
    protected function course_overview($courses, $overviews, $termcode) {
        $html = '';
        $html .= html_writer::start_tag('div', array('class' => '', 'id' => $termcode . '_courses'));
        if (empty($courses)) {
            $html .= $this->output->box_start('coursebox');
            $html .= $this->output->box(get_string('nocourses','block_course_overview_uwmoodle'), 'notify');
            $html .= $this->output->box_end();
        } else {
            foreach ($courses as $key => $course) {

                $html .= $this->output->box_start('coursebox');
                $html .= html_writer::start_tag('div', array('class' => 'course_title'));

                // No need to pass title through s() here as it will be done automatically by html_writer.
                $attributes = array('title' => $course->fullname);
                if (empty($course->visible)) {
                    $attributes['class'] = 'dimmed';
                }
                if(get_class($course) == 'external_course'){ // external courses have their urls already defined
                    $courseurl = new moodle_url($course->courseurl);
                    $lms = $course->LMS;
                }else{
                    $courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
                    $lms = "Moodle";
                }
                $coursefullname = format_string($course->fullname, true, $course->id);
                $link = html_writer::link($courseurl, $coursefullname, $attributes);
                $html .= $this->output->heading($link, 3, 'title ' . $lms . '-icon');
                //$html .= $this->output->box('', 'flush');
                $html .= html_writer::end_tag('div');

                if (isset($overviews[$course->id])) {
                    $html .= $this->activity_display($course->id, $overviews[$course->id], $termcode);
                }

                //$html .= $this->output->box('', 'flush');
                $html .= $this->output->box_end();
            }
        }
        $html .= html_writer::end_tag('div');
        return $html;
    }

    /**
     * Begin output of course_block
     *
     * @param string $title header text
     * @param string $id css optional id for div
     * @param string $helpidentifier optional help identifier
     * @param string $otherclasses Additional classes to add to the container
     * @return string
     */
    protected function begin_course_block($title, $id='', $helpidentifier='', $otherclasses = '') {

        $html  = '';

        // add space in front of first other class to fit into the attributes if there are other classes
        if(!empty($otherclasses)){
            $otherclasses = ' ' . $otherclasses;
        }
        $attributes = array('class' => 'courseblock clearfix' . $otherclasses);
        if (!empty($id)) {
            $attributes['id'] = $id;
        }
        $html .= html_writer::start_tag('div', $attributes);
        $html .= html_writer::start_tag('div', array('class' => 'cbheader'));
        $help = '';
        if ($helpidentifier) {
            $help = $this->output->help_icon($helpidentifier, 'block_course_overview_uwmoodle');
        }
        $html .= $this->output->heading($title.$help, 2, 'sectiontitle help');
        $html .= html_writer::end_tag('div');
        $html .= html_writer::start_tag('div', array('class' => 'cbcontent'));
        return $html;
    }

    /**
     * End output of course_block.
     *
     * @return string
     */
    protected function end_course_block() {
        $html  = '';
        $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');
        return $html;
    }

    /**
     * Render block footer
     *
     * @return string html to be displayed in course_overview block
     */
    public function footer() {
        global $CFG;
        $output = html_writer::tag('a', get_string("fulllistofcourses"), array('href' => "$CFG->wwwroot/course/index.php"));
        $output .= ' ...';
        $output .= $this->write_overviewids();
        return $output;
    }

    /**
     * Write uwmm_course_overviewids js variable with the activity overview id strings for js to manipulate display
     * of dom elements
     *
     * @return string HTMl string of script that writes the uwmm_course_overviewids
     */
    public function write_overviewids(){

        $output = '';
        $output .= '<script type="text/javascript">';
        $output .= 'var uwmm_course_overviewids = ' . json_encode($this->overviewids) . ';';
        $output .= '</script>';
        return $output;
    }

    /**
     * Coustuct activities overview for a course
     *
     * @param int $cid course id
     * @param array $overview overview of activities in course
     * @return string html of activities overview
     */
    protected function activity_display($cid, $overview, $id) {
        $output = html_writer::start_tag('div', array('class' => 'activity_info'));
        foreach (array_keys($overview) as $module) {
            $output .= html_writer::start_tag('div', array('class' => 'activity_overview'));
            $url = new moodle_url("/mod/$module/index.php", array('id' => $cid));
            $modulename = get_string('modulename', $module);
            $icontext = html_writer::link($url, $this->output->pix_icon('icon', $modulename, 'mod_'.$module, array('class'=>'iconlarge')));
            if (get_string_manager()->string_exists("activityoverview", $module)) {
                $icontext .= get_string("activityoverview", $module);
            } else {
                $icontext .= get_string("activityoverview", 'block_course_overview_uwmoodle', $modulename);
            }

            // Create activity overview section
            $output .= html_writer::tag('div', $icontext, array('id'=> 'uwmm_region_'.$id.'_'.$cid.'_'.$module . '_title'));
            $output .= html_writer::tag('div', $overview[$module], array('id'=>'uwmm_region_'.$id.'_'.$cid.'_'.$module . '_contents'));
            // Add overviewid to array to print out ids as an array for javascript
            $this->overviewids[] = 'uwmm_region_'.$id.'_'.$cid.'_'.$module;

            $output .= html_writer::end_tag('div');
        }
        $output .= html_writer::end_tag('div');
        return $output;
    }


    /**
     * Cretes html for welcome area
     *
     * @param int $msgcount number of messages
     * @return string html string for welcome area.
     */
    public function welcome_area($msgcount) {
        global $USER;
        $output = $this->output->box_start('welcome_area');

        $picture = $this->output->user_picture($USER, array('size' => 75, 'class' => 'welcome_userpicture'));
        $output .= html_writer::tag('div', $picture, array('class' => 'profilepicture'));

        $output .= $this->output->box_start('welcome_message');
        $output .= $this->output->heading(get_string('welcome', 'block_course_overview_uwmoodle', $USER->firstname));

        $plural = 's';
        if ($msgcount > 0) {
            $output .= get_string('youhavemessages', 'block_course_overview_uwmoodle', $msgcount);
        } else {
            $output .= get_string('youhavenomessages', 'block_course_overview_uwmoodle');
            if ($msgcount == 1) {
                $plural = '';
            }
        }
        $output .= html_writer::link(new moodle_url('/message/index.php'), get_string('message'.$plural, 'block_course_overview_uwmoodle'));
        $output .= $this->output->box_end();
        $output .= $this->output->box('', 'flush');
        $output .= $this->output->box_end();


        return $output;
    }

    /**
     * Renders the news
     *
     * @param string $news
     * @return string HTML of the news section
     */
    public function render_news($news){

        $output = $this->output->box_start('notification_area news_bg', 'news_area' );
        $output .= html_writer::tag('h3', get_string('news_header', 'block_course_overview_uwmoodle'), array('class'=>'notification_header news_h'));

        $output .= html_writer::start_tag('div', array('class' => 'notifications_container'));
        $output .= $news;
        $output .= html_writer::end_tag('div');
        $output .= $this->output->box_end();
        return $output;

    }

    /**
     * There are 2 instances in which this function is used
     *  -  When the block is functioning in AJAX mode and using the callback, all this function will do
     *      will return the html of the error container to let JS put the error message into the container
     *  -  When the block is functioning in PHP mode this function will recieve errors if there are any.  And if there are
     *      call error_log() on them to add them to the error log as well add them to the error container
     *
     * @param string $usephp Whether or not to show the section, as in php mode there is no js to show the block
     * @param array $errors An array of errors given by external courses
     * @return string HTML string of the error container with possible errors
     */
    public function render_error_container($usephp, $errors = array()){

        if(empty($usephp)){
            $showcontainer = 'hidecontainer';
        }else{

            if(empty($errors)){
                $showcontainer = 'hidecontainer';
            }else{
                $showcontainer = '';
            }
        }

        $output = $this->output->box_start('notification_area warning_bg ' . $showcontainer, 'warning_area' );
            $output .= html_writer::tag('h3', get_string('warning_header', 'block_course_overview_uwmoodle'), array('class'=>'notification_header warning_h'));

            $output .= html_writer::start_tag('div', array('class' => 'notifications_container', 'id'=>'warning_container'));

            if(!empty($errors)){

                foreach($errors as $error){
                    error_log($error);
                }

                if(debugging() && is_siteadmin()){ // if debugging show actual error messages

                    $output .= html_writer::alist($errors);

                }else{ // show general message
                    $output .= get_string('general_warning_message', 'block_course_overview_uwmoodle');
                }

            }
            $output .= html_writer::end_tag('div');
        $output .= $this->output->box_end();
        return $output;


    }

    /**
     * Renders legend section
     *
     * @return string HTML of the legend
     */
    public function render_create_course_and_legend(){
        global $CFG;
        $output = '';

        $output .= html_writer::start_tag('div', array('class', 'course_overview_info'));

            if(file_exists($CFG->dirroot . '/enrol/wisc/accesslib.php')){
                require_once($CFG->dirroot . '/enrol/wisc/accesslib.php');
            }

            if(function_exists('wisc_can_create_course')){
                if(wisc_can_create_course()){

                    $createcourselink = new moodle_url('/enrol/wisc/create.php');
                    $output .= html_writer::start_tag('div', array('class'=>'createcourse'));
                    $output .= html_writer::link($createcourselink,
                                                 get_string('create_course', 'block_course_overview_uwmoodle'));
                    $output .= html_writer::end_tag('div');

                }
            }

            $output .= html_writer::start_tag('div', array('class'=>'legend'));
            $output .= html_writer::tag('div', get_string('moodle_legend', 'block_course_overview_uwmoodle'), array('class'=>'Moodle-icon'));
            //$output .= html_writer::tag('div', get_string('d2l_legend', 'block_course_overview_uwmoodle'), array('class'=>'D2L-icon'));
            $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
        return $output;
    }

    /**
     * The course block as it renders when using the AJAX functionality
     *
     * @return string
     */
    public function ajax_course_block(){
        $html  = '';
        $html .= $this->begin_course_block(get_string('courses', 'block_course_overview_uwmoodle'), 'uwmm_mycourses_block');
        $html .= html_writer::start_tag('div', array('class' => 'rhs'));
        $html .= $this->ajax_terms_course_overview();
        $html .= html_writer::end_tag('div');
        $html .= $this->end_course_block();
        return $html;
    }

    /**
     *
     *
     * @return string
     */
    public function ajax_terms_course_overview(){
        global $PAGE;
        $html = '';

        $html .= html_writer::start_tag('div', array('class' => 'courselistcontainer', 'id' => 'uwmm_terms_content'));

        // add in loading courses and no javascript divs for display on the block when using ajax
        $html .= html_writer::tag('div', '', array('class'=>'loadingcourses'));
        $html .= html_writer::start_tag('div', array('class'=>'nojavascript'));
            $usephpurl = clone($PAGE->url);
            $usephpurl->param('usephp', 'true');
            $html .= html_writer::link($usephpurl, get_string('nojavascript', 'block_course_overview_uwmoodle'));
        $html .= html_writer::end_tag('div');


        $html .= html_writer::end_tag('div');

        return $html;

    }

    /**
     * Builds the container for the other courses section
     *
     * @return string
     */
    public function ajax_other_course_block(){
        $html  = '';
        $html .= $this->begin_course_block(get_string('ongoingcourses', 'block_course_overview_uwmoodle'), 'uwmm_othercourses_block', 'ongoingcourses', 'hidden');
        $html .= html_writer::tag('div', '', array('id'=>'ongoing_courses'));
        $html .= $this->end_course_block();
        return $html;
    }

    /**
     * Adds in script tag for using ajax or not
     *
     * @param bool $useajax
     *
     * @return string
     */
    public function use_ajax($useajax, $config = ''){
        global $CFG;

        $html = '<script type="text/javascript">';

        if($useajax){
            $html .= 'block_course_overview_uwmoodle_use_ajax = true;';
            $html .= 'block_course_overview_uwmoodle_config = \'' . json_encode($config) . '\';';
        }else{
            $html .= 'block_course_overview_uwmoodle_use_ajax = false;';
        }
        $html .= '</script>';
        return $html;
    }
}
