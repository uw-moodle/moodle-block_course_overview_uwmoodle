<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper functions for course_overview block
 *
 * @package    block_course_overview_uwmoodle
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Adapted for course_overview_uwmoodle
 *
 * @author 2013 Matt Petro
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Display overview for courses
 *
 * @param array $courses courses for which overview needs to be shown
 * @return array html overview
 */
function block_course_overview_uwmoodle_get_overviews($courses) {
    $htmlarray = array();
    if ($modules = get_plugin_list_with_function('mod', 'print_overview')) {
        // Split courses list into batches with no more than MAX_MODINFO_CACHE_SIZE courses in one batch.
        // Otherwise we exceed the cache limit in get_fast_modinfo() and rebuild it too often.
        if (defined('MAX_MODINFO_CACHE_SIZE') && MAX_MODINFO_CACHE_SIZE > 0 && count($courses) > MAX_MODINFO_CACHE_SIZE) {
            $batches = array_chunk($courses, MAX_MODINFO_CACHE_SIZE, true);
        } else {
            $batches = array($courses);
        }
        foreach ($batches as $courses) {
            foreach ($modules as $fname) {
                $fname($courses, $htmlarray);
            }
        }
    }
    return $htmlarray;
}

/**
 * Returns shortname of activities in course
 *
 * @param int $courseid id of course for which activity shortname is needed
 * @return string|bool list of child shortname
 */
function block_course_overview_uwmoodle_get_child_shortnames($courseid) {
    global $DB;
    $ctxselect = context_helper::get_preload_record_columns_sql('ctx');
    $sql = "SELECT c.id, c.shortname, $ctxselect
            FROM {enrol} e
            JOIN {course} c ON (c.id = e.customint1)
            JOIN {context} ctx ON (ctx.instanceid = e.customint1)
            WHERE e.courseid = :courseid AND e.enrol = :method AND ctx.contextlevel = :contextlevel ORDER BY e.sortorder";
    $params = array('method' => 'meta', 'courseid' => $courseid, 'contextlevel' => CONTEXT_COURSE);

    if ($results = $DB->get_records_sql($sql, $params)) {
        $shortnames = array();
        // Preload the context we will need it to format the category name shortly.
        foreach ($results as $res) {
            context_helper::preload_from_record($res);
            $context = context_course::instance($res->id);
            $shortnames[] = format_string($res->shortname, true, $context);
        }
        $total = count($shortnames);
        $suffix = '';
        if ($total > 10) {
            $shortnames = array_slice($shortnames, 0, 10);
            $diff = $total - count($shortnames);
            if ($diff > 1) {
                $suffix = get_string('shortnamesufixprural', 'block_course_overview_uwmoodle', $diff);
            } else {
                $suffix = get_string('shortnamesufixsingular', 'block_course_overview_uwmoodle', $diff);
            }
        }
        $shortnames = get_string('shortnameprefix', 'block_course_overview_uwmoodle', implode('; ', $shortnames));
        $shortnames .= $suffix;
    }

    return isset($shortnames) ? $shortnames : false;
}

/**
 * Return sorted list of user courses
 *
 * @return array list of sorted courses grouped by term.
 */
function block_course_overview_uwmoodle_get_sorted_courses() {
    global $USER, $CFG, $DB;

    $courses = enrol_get_my_courses(null, 'fullname', 0);
    $site = get_site();

    if (array_key_exists($site->id,$courses)) {
        unset($courses[$site->id]);
    }

    foreach ($courses as $c) {
        if (isset($USER->lastcourseaccess[$c->id])) {
            $courses[$c->id]->lastaccess = $USER->lastcourseaccess[$c->id];
        } else {
            $courses[$c->id]->lastaccess = 0;
        }
    }

    // Find the termcode for each course
    if (!empty($courses)) {
        $courseids = array();
        foreach ($courses as $course) {
            $courseids[] = $course->id;
        }
        list ($insql, $inparams) = $DB->get_in_or_equal($courseids);
        $select = "enrol='wisc' AND courseid $insql";
        $enrols = $DB->get_records_select('enrol', $select, $inparams, '', 'id,courseid,customchar1');
        foreach ($enrols as $enrol) {
            if (empty($courses[$enrol->courseid]->term) || $courses[$enrol->courseid]->term < $enrol->customchar1) {
                $courses[$enrol->courseid]->term = $enrol->customchar1;
            }
        }
    }

    // get external courses
    require_once($CFG->dirroot . '/enrol/wisc/lib/externalcourses/external_user_courses.php');
    $externalcourses = new external_user_courses();
    $ecourses = $externalcourses->get_user_courses($USER->username, $courses);

    $courses = array_merge($courses, $ecourses);

    return array($courses, $externalcourses->get_errors());
}

/**
 * Organize courses into terms, maintaining existing sorting inside each term
 *
 * @param array $courses user courses
 * @return array
 */
function block_course_overview_uwmoodle_group_courses_by_term($courses) {
    global $DB;

    // Organize courses into terms, maintaining existing sorting inside each term
    $terms = array();
    foreach ($courses as $course) {
        if (!empty($course->term)) {
            $terms[$course->term][$course->id] = $course;
        } else {
            $terms[block_course_overview_uwmoodle::TERM_OTHER][$course->id] = $course;
        }
    }

    return $terms;
}

/**
 * Sort term array by ascending date
 *
 * @param array $terms keys are term_codes
 * @return boolean
 */
function block_course_overview_uwmoodle_sort_term_array(&$terms) {

    // Sort the terms with ongoing (code = 0) first, then in decreasing order
    $cmp = function ($a, $b) {
               if ($a == 0 || $b == 0) {
                   return $a-$b;
               } else {
                   return $b-$a;
            }
    };

    return uksort($terms, $cmp);
}

/**
 * Return a string representing the term (e.g. "Fall 2010")
 * This function doesn't make any remote calls.
 *
 * @param string $termCode
 * @return string $termName
 */
function block_course_overview_uwmoodle_get_term_name($termCode) {
    $termCode = (string)$termCode;

    $c = substr($termCode,0,1);
    $yy = substr($termCode,1,2);
    $year = 1900+100*$c+$yy;
    $semester = substr($termCode,3,1);
    switch($semester) {
        case 2:
            $name = sprintf("<span class='semester'>Fall</span>&nbsp;<span class='year'>%d</span>", $year-1);
            break;
        case 3:
            $name = sprintf("<span class='semester'>Winter</span>&nbsp;<span class='year'>%d</span>", $year);
            break;
        case 4:
            $name = sprintf("<span class='semester'>Spring</span>&nbsp;<span class='year'>%d</span>", $year);
            break;
        case 6:
            $name = sprintf("<span class='semester'>Summer</span>&nbsp;<span class='year'>%d</span>", $year);
            break;
        default:
            $name = "Ongoing";
    }
    return $name;
}