<?php

/**
 * Course overview block for UW Moodle
 *
 * per-instance configuration
 *
 * @author 2013 Matt Petro
 */

defined('MOODLE_INTERNAL') || die;

class block_course_overview_uwmoodle_edit_form extends block_edit_form {

    protected function specific_definition($mform) {

        // Section header title according to language file.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

        // Should the ongoing courses be merged into the term menu?
        $mform->addElement('advcheckbox', 'config_combineongoing', get_string('combineongoing', 'block_course_overview_uwmoodle'), null, null, array(0, 1));
        $mform->setDefault('config_nontermseparate', 0);
    }
}