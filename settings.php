<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * course_overview block settings
 *
 * @package    block_course_overview_uwmoodle
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @copyright  2014 Univrsity of Wisconsin System - Board of Regents
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configcheckbox('block_course_overview_uwmoodle/showwelcomearea', new lang_string('showwelcomearea', 'block_course_overview_uwmoodle'),
        new lang_string('showwelcomeareadesc', 'block_course_overview_uwmoodle'), 1));

    // my page news setting.
    $name = 'block_course_overview_uwmoodle/news';
    $title = get_string('news', 'block_course_overview_uwmoodle');
    $description = get_string('news_desc', 'block_course_overview_uwmoodle');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $settings->add($setting);
}
