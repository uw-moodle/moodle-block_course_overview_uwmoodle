
function couwmm_create_warning_list(warnings){

    if(warnings.length == 0){
        return ''; // return nothing for no errors array
    }

    var returnHTML = '<ul>';
    $.each(warnings, function(index, value){
        returnHTML += '<li>' + value + '</li>';
    });

    returnHTML += '</ul>';
    return returnHTML;
}


function create_course_box(course){
    'use strict';

    var externalcourse = ' internal-course';
    if(course.externalcourse === 1){
        externalcourse = ' external-course';
    }

    var dimmed = '';
    if(course.visible == 0){
        dimmed = ' class="dimmed" ';
    }

    var coursebox = '<div class="box coursebox"><div class="course_title"><h3 class="title '+ course.lms + '-icon '+ externalcourse + '">';
    coursebox += '<a title="' + course.name + '" href="' + course.url + '" ' + dimmed + '>' + course.name + '</a>';
    coursebox += '</h3></div></div>';
    return coursebox;
}

function no_courses(message){
    'use strict';

    return '<div class="box coursebox"><div class="box notify">' + message + '</div></div>';

}

function create_term_section(term, no_course_message){
    'use strict';

    var html = '<h2 class="termname">' + term.termname + '</h2>';
    html += '<div id="' + term.termcode + '_courses">';

    if(term.courses.length > 0){
        $.each(term.courses, function(index, course){
            html += create_course_box(course);
        });
    }else{
        html += no_courses(no_course_message);
    }

    html += '</div>';
    return html;
}



$(document).ready(function(){


    if(window.block_course_overview_uwmoodle_use_ajax){ // if we're using ajax

        $.post(M.cfg.wwwroot + '/blocks/course_overview_uwmoodle/getcourses.php', {sesskey: M.cfg.sesskey}, function(response){

            var termHTML = ''; // create an html string to hold all of the terms HTML

            var terms = new Array();
            $.each(response.termcourses, function(index, term){
               termHTML += create_term_section(term, response.nocoursesmessage);

                // because terms are already sorted, we'll put each on in an array and then make the current one
                // active after creating the accordion
                terms.push(term.termcode);
            });

            var active = null;
            // go through the terms array to find an index to make active in the accordion
            $.each(terms, function(index, termcode){
                if(termcode == response.selectedterm){
                    active = index;
                }
            });

            $('#uwmm_terms_content').html(termHTML);
            $('#uwmm_terms_content').accordion({ header: "h2", heightStyle: "content", active: active});

            // next is ongoing courses
            if(response.othercourses !== undefined){

                if(response.othercourses.length > 0){

                    $('#uwmm_othercourses_block').show();
                    var ongoingHTML = '';
                    $.each(response.othercourses, function(index, course){
                        ongoingHTML += create_course_box(course);
                    });
                    $('#ongoing_courses').html(ongoingHTML);
                }
            }

            if(response.errors !== undefined){

                var warningMsg = '';
                if( Object.prototype.toString.call( response.errors ) === '[object Array]' ) {
                    // we have a list of errors
                    warningMsg = couwmm_create_warning_list(response.errors);

                }else{
                    // we have a general warning string
                    warningMsg = response.errors;
                }

                if(warningMsg.length > 0){ // if there is a warning message show it
                    document.getElementById('warning_container').innerHTML = warningMsg;
                    $('#warning_area').show();
                }
            }

        }, 'json');

    }else{
        $('#uwmm_terms_content').accordion({ header: "h2", heightStyle: "content"});
    }


    // not used as we are not using course overviews
    /*$.each(window.uwmm_course_overviewids, function(index, value){
        $('#' + value + '_contents').hide();
        $('#' + value + '_title > a').click(function(e){
            e.preventDefault();
        });

        $('#' + value + '_title').click(function(e){
            $('#' + value + '_contents').slideToggle();
        });
    });*/
});