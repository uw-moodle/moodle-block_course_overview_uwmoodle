<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course overview block for UW Moodle
 *
 * This block improves on the default course_summary block by filtering courses by term.
 *
 * The block is dependent on the UW enrollment plugins and is largely coped from the existing course_overview block.
 * I've left copyright statements for the original authors on most files.
 *
 * @author 2013 Matt Petro
 * @author 2014 John Hoopes
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/blocks/course_overview_uwmoodle/locallib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/blocks/moodleblock.class.php');

class block_course_overview_uwmoodle extends block_base {
    /**
     * block initializations
     */
    public function init() {
    global $CFG, $PAGE;

        $this->title   = get_string('pluginname', 'block_course_overview_uwmoodle');
        if($CFG->version < '2013051400'){
            $PAGE->requires->css('/blocks/course_overview_uwmoodle/js/jquery-ui.min.css');
        }

    }

    /**
     * Require jQuery and jQuery ui plugins for rendering the block
     */
    public function get_required_javascript() {
        global $CFG;

        parent::get_required_javascript();

        if($CFG->version >= '2013051400') {
            $this->page->requires->jquery();
            $this->page->requires->jquery_plugin('ui');
            $this->page->requires->jquery_plugin('ui-css');
            $this->page->requires->jquery_plugin('block_course_overview_uwmoodle-animate-enhanced', 'block_course_overview_uwmoodle');
        }else{
            $this->page->requires->js('/blocks/course_overview_uwmoodle/js/jquery-ui.min.js');
        }
        $this->page->requires->js('/blocks/course_overview_uwmoodle/js/course_overview_uwmoodle.js');
    }

    const TERM_OTHER = 0;  // our "term_code" for non-timetable classes

    /**
     * block contents
     *
     * @return object
     */
    public function get_content() {
        global $USER, $CFG, $DB;

        if($this->content !== NULL) {
            return $this->content;
        }

        $config = get_config('block_course_overview_uwmoodle');

        $this->content = new stdClass();
        $this->content->text = '';

        profile_load_custom_fields($USER);

        // start rendering the welcome section and then determine if this is AJAX load or php load
        $renderer = $this->page->get_renderer('block_course_overview_uwmoodle');
        if (!empty($config->showwelcomearea)) {
            require_once($CFG->dirroot.'/message/lib.php');
            $msgcount = message_count_unread_messages();
            $this->content->text = $renderer->welcome_area($msgcount);
        }
        // next render the news section if there is any content
        if(!empty($config->news)){
            $this->content->text .= $renderer->render_news($config->news);
        }

        $usephp = optional_param('usephp', '', PARAM_ALPHA);
        if(empty($usephp)){ // set up course blocks, but then let ajax load courses
            // the next 2 renders happen for both, but to get the order to display correctly they have to be put in twice

            // render the error container so that js/php can add errors
            $this->content->text .= $renderer->render_error_container($usephp);

            // render the legend
            $this->content->text .= $renderer->render_create_course_and_legend();

            $this->content->text .= $renderer->use_ajax(true, $this->config); // adds in a javascript variable for using ajax
            $this->content->text .= $renderer->ajax_course_block();

            $this->content->text .= $renderer->ajax_other_course_block();
        }else{ // load courses without using ajax to load them on the client side
            $this->content->text .= $renderer->use_ajax(false); // adds in javascript variable to not use ajax

            // Get user's courses and sort by term
            list($courses, $errors) = block_course_overview_uwmoodle_get_sorted_courses();
            $terms = block_course_overview_uwmoodle_group_courses_by_term($courses);

            $currentterm = \enrol_wisc\local\chub\timetable_util::get_current_termcode();
            $selectedterm = optional_param('term', $currentterm, PARAM_INT);

            if (!isset($terms[$currentterm])) {
                $terms[$currentterm] = array();
            }
            // If selectedterm is not valid, select currentterm
            if (!isset($terms[$selectedterm])) {
                $selectedterm = $currentterm;
            }

            // Get course overviews
            //$overviews = block_course_overview_uwmoodle_get_overviews($courses);
            $overviews = array(); // for now course overviews are disabled

            // Sort the terms with newest first
            block_course_overview_uwmoodle_sort_term_array($terms);

            $displayedterms = $terms;
            $othercourses = false;
            if (empty($this->config->combineongoing)) {
                unset($displayedterms[self::TERM_OTHER]);
                if (!empty($terms[self::TERM_OTHER])) {
                    $othercourses = $terms[self::TERM_OTHER];
                }
            }

            /// the next 2 renders happen for both, but to get the order to display correctly they have to be put in twice
            // render the error container so that js/php can add errors
            $this->content->text .= $renderer->render_error_container($usephp, $errors);

            // render the legend
            $this->content->text .= $renderer->render_create_course_and_legend();

            // Render the block
            $this->content->text .= $renderer->course_block($displayedterms, $overviews, $selectedterm);

            if (!empty($othercourses)) {
                $this->content->text .= $renderer->other_course_block($othercourses, $overviews);
            }
        }

        $this->content->footer = $renderer->footer();
        return $this->content;
    }

    /**
     * allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
        return array('my-index'=>true);
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        // Hide header if welcome area is show.
        $config = get_config('block_course_overview_uwmoodle');
        return !empty($config->showwelcomearea);
    }
}
