<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax callback for the course_overview_uwmoodle block
 * Gets the user's courses and returns it to the javascript in the page
 *
 * @package    block_course_overview_uwmoodle
 * @author	   2014 John Hoopes
 * @copyright  University of Wisconsin System - Board of Regents
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// define that this is an ajax script
define('AJAX_SCRIPT', 1);

require_once("../../config.php");

require_login();

$response = new stdClass();
$response->status = 'false';
$response->message = 'invalidrequest';

$PAGE->set_context(context_system::instance());

// Unlock session during potentially long webservice requests.
\core\session\manager::write_close();

if(confirm_sesskey()) {

    // now require the block lib files
    require_once($CFG->dirroot.'/blocks/course_overview_uwmoodle/block_course_overview_uwmoodle.php');

    define('TERM_OTHER', 0);
    $blockconfig = optional_param('blockconfig', new stdClass(), PARAM_RAW);

    if(empty($blockconfig->combineongoing)){ // set conbineongoing to something for code later
        $blockconfig->combineongoing = false;
    }

    profile_load_custom_fields($USER);

    // Get user's courses and sort by term
    list($courses, $errors) = block_course_overview_uwmoodle_get_sorted_courses();

    if(!empty($errors)){
        if(debugging() && is_siteadmin()){
            $response->errors = $errors;
        }else{
            $response->errors = get_string('general_warning_message', 'block_course_overview_uwmoodle');
        }
    }

    $terms = block_course_overview_uwmoodle_group_courses_by_term($courses);

    $currentterm = \enrol_wisc\local\chub\timetable_util::get_current_termcode();
    $selectedterm = optional_param('term', $currentterm, PARAM_INT);

    if (!isset($terms[$currentterm])) {
        $terms[$currentterm] = array();
    }
    // If selectedterm is not valid, select currentterm
    if (!isset($terms[$selectedterm])) {
        $selectedterm = $currentterm;
    }

    // Get course overviews
    //$overviews = block_course_overview_uwmoodle_get_overviews($courses);
    $overviews = array(); // for now course overviews are disabled

    // Sort the terms with newest first
    block_course_overview_uwmoodle_sort_term_array($terms);

    $displayedterms = $terms;
    $othercourses = false;
    if (empty($blockconfig->combineongoing)) {
        unset($displayedterms[block_course_overview_uwmoodle::TERM_OTHER]);
        if (!empty($terms[block_course_overview_uwmoodle::TERM_OTHER])) {
            $othercourses = $terms[block_course_overview_uwmoodle::TERM_OTHER];
        }
    }

    $returntermcourses = array();
    // build return course array
    foreach($displayedterms as $termcode => $courses){

        $term = new stdClass(); // store the term code, term name, and term courses in an object
        $term->termcode = $termcode;
        $term->termname = $termstr = block_course_overview_uwmoodle_get_term_name($termcode);
        $term->courses = array();

        foreach($courses as $course){
            $returncourse = new stdClass();
            $returncourse->name = $course->fullname;
            if(get_class($course) == 'external_course'){ // external courses have their urls already defined
                $courseurl = new moodle_url($course->courseurl);
                $returncourse->lms = $course->LMS;
                $returncourse->externalcourse = 1;
            }else{
                $courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
                $returncourse->lms = 'Moodle'; // local courses are always Moodle
                $returncourse->externalcourse = 0;
            }
            $returncourse->url = $courseurl->out();
            $returncourse->visible = $course->visible;
            $term->courses[] = $returncourse;
        }
        $returntermcourses[] = $term;
    }

    $returnothercourses = array();
    if($othercourses !== false){ // if there are other courses build the array of them

        foreach($othercourses as $course){
            $returncourse = new stdClass();
            $returncourse->name = $course->fullname;
            if(get_class($course) == 'external_course'){ // external courses have their urls already defined
                $courseurl = new moodle_url($course->courseurl);
                $returncourse->lms = $course->LMS;
                $returncourse->externalcourse = 1;
            }else{
                $courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
                $returncourse->lms = 'Moodle'; // local courses are always Moodle
                $returncourse->externalcourse = 0;
            }
            $returncourse->url = $courseurl->out();
            $returncourse->visible = $course->visible;
            $returnothercourses[] = $returncourse;
        }

    }

    $response->status = 'true';
    $response->message = 'success';
    $response->selectedterm = $selectedterm;
    $response->termcourses = $returntermcourses;
    $response->othercourses = $returnothercourses;
    $response->nocoursesmessage = get_string('nocourses','block_course_overview_uwmoodle');
}


echo json_encode($response);


